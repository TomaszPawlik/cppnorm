//============================================================================
// Name        : CPP.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstring>
#include <random>
#include <fstream>
#include <string>
#include <chrono>

void* myMemCpy (void* aDestMemory, void* const aSourceMemory, size_t aNumberOfBToCopy, size_t aSizeOfDest ,size_t aSizeOfSource)
{

	/*
	 * Funkcja powinna rozpatrzyć następujące przypadki:
	 *
	 * (1) Dest = Source						Moim zdaniem: return aSourceMemory;
	 * (2) Source = nullptr;					Moim zdaniem: throw
	 * (3) Dest = nullptr;						Moim zdaniem: zrobienie new na rozmiar aNumberOfBytesToCopy
	 * (4) aNumberOfBToCopy > aSizeOfSource		Moim zdaniem: throw
	 * (5) aNumberOfBToCopy > aSizeOfDest		Moim zdaniem: throw
	 * (6) aNumberOfBToCopy < aSizeOfDest		OK - wykonujemy operacje - czyli to co jest zaimplementowane na dole
	 * (7) aNumberOfBToCopy < 0					Moim zdaniem: throw
	 *
	 */

	uint8_t * sourceMemoryUintPtr = static_cast<uint8_t*>(aSourceMemory);
	uint8_t * destMemoryUintPtr = static_cast<uint8_t*>(aDestMemory);

	for (size_t index = 0; index < aNumberOfBToCopy; ++index)
	{
		destMemoryUintPtr[index] = sourceMemoryUintPtr[index];
	}

	return static_cast<void*>(aDestMemory);
}

char * myStrCpy ( char * strTo, const char * strFrom )
{

	/*
	 * Funkcja powinna rozpatrzyć następujące przypadki:
	 *
	 * (1) strTo = strFrom								Moim zdaniem: return strFrom;
	 * (2) strFrom = nullptr;							Moim zdaniem: throw że strFrom to nullptr;
	 * (3) strTo = nullptr;								Robimy nowy wskaznik na ilosc elementów strFrom+1 (na '\0')
	 * (4) ilość elementów strFrom  > zakres strTo		Chyba nie można sprawdzić mając podany jedynie wskaźnik
	 * 													Sytuacja niepozadana
	 * (5) ilość elementów strFrom < zakres strTo		OK - wykonujemy operacje
	 *
	 */

	if ( strTo == strFrom )				// (1)
	{
		return strTo;
	}
	else if (strFrom == nullptr)		// (2)
	{
		throw "myStrCpy error - can not copy from nullptr";
	}

	int counterForFromCharTab = 0;		// Zliczenie ilosci znaków w tablicy From
	for (int index = 0; strFrom[index] != '\0'; ++index)
	{
		counterForFromCharTab++;
	}

	if (nullptr == strTo)				// (3)
	{
		strTo = new char[counterForFromCharTab+1];
	}

	for (int index = 0; strFrom[index] != '\0'; ++index) // (5)
	{
		strTo[index] = strFrom[index];
	}

	return strTo;
}

void* myMemSet( void* dest, int ch, std::size_t count )
{

	/*
	 * Funkcja powinna rozpatrzyć następujące przypadki:
	 *
	 * (1) count <= 0									return dest;
	 * (2) dest = nullptr;								return nullptr;
	 * (3) przypisany zakres dest  < count				Chyba nie można sprawdzić mając podany jedynie wskaźnik
	 * 													Sytuacja niepozadana
	 * (4) przypisany zakres dest >= count				OK - wykonujemy operacje
	 *
	 */

	if ( 0 <= count )				// (1)
	{
		return static_cast<void*>(dest);
	}
	if ( nullptr == dest)			// (2)
	{
		return nullptr;
	}

	uint8_t * destMemoryUintPtr = static_cast<uint8_t*>(dest);
	for (size_t index = 0; index < count; ++index)						// (4)
	{
		destMemoryUintPtr[index] = ch;
	}

	return static_cast<void*>(dest);
}

int main() {

	// Task 5.8 // memcpy ////////////////////////////////////////////////
	// ///////////////////////////////////////////////////////////////////

	constexpr int destTableSize = 10;

	char dest[destTableSize];
	char source[] = "ZASa";

	myMemCpy(dest,source,9,sizeof(dest),sizeof(source));

	for (int index = 0; dest[index] != '\0'; ++index)
	{
		std::cout << dest[index] << " ";
	}
	std::cout << std::endl;

	// Task 6.1 // // strcpy /////////////////////////////////////////////
	// ///////////////////////////////////////////////////////////////////

	char dest2[destTableSize];
	char source2[] = "KSfgd";

	myStrCpy(dest2,source2);

	for (int index = 0; dest2[index] != '\0'; ++index)
	{
		std::cout << dest2[index] << " ";
	}
	std::cout << std::endl;

	// Task 6.2 // // memset /////////////////////////////////////////////
	// ///////////////////////////////////////////////////////////////////

	char source3[] = "First word";
	myMemSet(source3,'-',3);

	for (int index = 0; source3[index] != '\0'; ++index)
	{
		std::cout << source3[index] << " ";
	}
	std::cout << std::endl;

	// Task 6.3 // // zapis do pliku, rand i time ////////////////////////
	// ///////////////////////////////////////////////////////////////////

	constexpr int numberOfIntsToGenerate = 10;

	// Tools for random number generation.
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist(0,10); // distribution in range [1, 10]

    // File make and save, time start
    auto start = std::chrono::system_clock::now();
    std::ofstream outfile;
    outfile.open("AAA.txt");

    for (int oneHundredInts = 0; oneHundredInts < numberOfIntsToGenerate; ++oneHundredInts)
    	outfile << dist(rng) << '\n';

    outfile.close();

    // File open, read and time stop

    std::string line;
    std::ifstream myfile ("AAA.txt");

    while ( std::getline (myfile,line) )
    {
      std::cout << line << std::endl;
    }
    myfile.close();
    auto end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end-start;

    std::cout << elapsed_seconds.count() << "s";
	return 0;
}
