//============================================================================
// Name        : CPP.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

unsigned int factorialDebug(unsigned int aVal){
	unsigned int sum;
	if (aVal == 0)
	{
		sum = 1;
		cout << aVal << "! : " << sum << endl;
	}
	else
	{
		sum = aVal * factorialDebug(aVal - 1);
		cout << aVal << "! : " << sum << endl;
	}
	return sum;
}

unsigned int factorialResult(unsigned int aVal){
	if (aVal==0)
		return 1;
	return factorialResult(aVal-1)*aVal;
}

unsigned int x=1;

int main() {

	// prekompilacja (g++ -E <nazwa pliku> - wykonanie dyrektyw preprocesora - #include, #define, #if, makra
	// kompilacja (g++ -c file.cpp) - zamiana kodu z języka programowania na język pośredni kompilatora a potem asemblera.
	//								  w przypadku błędów zostaną wyświetlone errry. Nastepuje również optymalizacja
	// linkowanie - zrobienie wykonywalnego programu

	// g++ -o task1.3-1.out CPP-Prog-3.cpp 					- bez definiowania DEBUG
	// g++ -o task1.3-2.out -D DEBUG CPP-Prog-3.cpp 		- zdefiniowanie DEBUG
	// g++ -c -D DEBUG CPP-Prog-3.cpp -Wall -o0				- kompilacja
	// g++ -c CPP-Prog-3.cpp -Wall -o3						- kompilacja


	cout << "Podaj lizbe dla ktorej zostanie obliczna silnia: ";
	cin >> x;

	#ifdef DEBUG
		factorialDebug(x);
	#else
		cout << "Wynik: " << factorialResult(x) << endl;
	#endif

}
