/*
// Task 5,7  ///////////////////////
////////////////////////////////////

constexpr int colSize = 4;
constexpr int rowSize = 3;
int ** cols = new int* [rowSize];

cols[0] = new int [colSize*rowSize];

for (int i = 1; i < rowSize; ++i)
{
	cols[i] = cols[0] + i*colSize;
}
*/

//============================================================================
// Name        : CPP.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstring>
#include <cstdint>

using namespace std;

void tableAddressAndValueDisplay(int *aTable, int aSizeOfTable)
{
	for (int tabIndex=0;tabIndex<aSizeOfTable;++tabIndex)
	{
		cout << aTable[tabIndex] << "  -  "<< &aTable[tabIndex] << endl;
	}
}

int* functionTaskTwo(int aPassByValue, int * aPassByPointer, int& aPassByReference)
{
	aPassByValue++;
	aPassByReference++;
	(*aPassByPointer)++;

	return aPassByPointer; //Wartosc bedzie miala dwa, bo aPassByValue jest kopia
}

struct Zwierze
{
	int mWiek;
	int mWzrost;
	std::string mNazwa;
	std::string mGatunek;
};

void przypiszDane(Zwierze& aObj)
{
	aObj.mWiek=8;
	aObj.mWzrost = 60;
	aObj.mNazwa = "Azor";
	aObj.mGatunek = "Pies";
}

int countNumberEncountersInArray(int* aArrayToCheck, int tableSize, int aNumberToCheck)
{
	int counter = 0;
	for (int index = 0; index < tableSize; ++index)
	{
		if (aNumberToCheck == aArrayToCheck[index])
		{
			++counter;
		}
	}
	return counter;
}

/*
void* myRealloc(void * aPointerOfDataToRealloc, size_t aOldDataSize , size_t aNewDataSize)
{
	void * helpPointer = (char*)malloc(aNewDataSize);
	memcpy(helpPointer, aPointerOfDataToRealloc, aOldDataSize);

	delete[] aPointerOfDataToRealloc;
	aPointerOfDataToRealloc = nullptr;

	return helpPointer;
}
*/

void* myRealloc (void* oldMemory, size_t oldSize, size_t newSize)
{
	if (oldSize == newSize)
	{
		return oldMemory;
	}
	if (0 == newSize)
	{
		delete[] oldMemory;
		return nullptr;
	}
	uint8_t * newMemory = new uint8_t[newSize];
	uint8_t * oldMemoryUintPtr = static_cast<uint8_t*>(oldMemory);
	const size_t elementsToCopy = (oldSize < newSize ? oldSize : newSize);
	for(size_t index = 0; index < elementsToCopy; ++index)
	{
		newMemory[index] = oldMemoryUintPtr[index];
	}
	delete[] oldMemoryUintPtr;
	oldMemory = nullptr;
	return static_cast<void*>(newMemory);
}


// Ver2 - overloaded [][] operator
class TwoDimArrayInOneSetVer2
{
public:
	TwoDimArrayInOneSetVer2(int aHeight, int aWidth) :
	mHeight(aHeight),
	mWidth(aWidth)
	{
		int totalSize = mWidth * mHeight;
		mPointerToData = new int[totalSize]{};

        for (int index = 0; index<totalSize; ++index)
            mPointerToData[index]=index;
	}

	~TwoDimArrayInOneSetVer2()
	{
		delete[] mPointerToData;
	}

        class Wrapper {
        public:
            Wrapper(int* heightPtr) : mWrapper(heightPtr) { }
            int& operator[](int indexWidth) {
                return *(mWrapper+indexWidth);
            }
        private:
            int * mWrapper;
        };

    Wrapper operator[](int indexHeight) {
        return Wrapper(&mPointerToData[indexHeight*mWidth]);
    }

private:
	int * mPointerToData;
	int mHeight;
	int mWidth;
};

	int main() {

	// ///////////////////////////////////////////////////////////////////////////////////////
	// Task 5.1 - wypisac wartosci z tablicy i ich adres

	int arr[]{1,2,3,4,5,6};
	tableAddressAndValueDisplay(arr,(sizeof(arr)/sizeof(*arr)));

	// ///////////////////////////////////////////////////////////////////////////////////////
	// Task 5.2

	int intValue = 0;
	int * intPointer = &intValue;

	cout << *functionTaskTwo(intValue,intPointer,intValue) << endl;
	// ///////////////////////////////////////////////////////////////////////////////////////
	// Task 5.3 - struktura zwierze, stworzyc przekazac do funkcji przypiszDane(), wypisac dane zwierzaka

	Zwierze zwierzeObj;
	przypiszDane(zwierzeObj);

	cout << "Wiek: " << zwierzeObj.mWiek << endl;
	cout << "Wzrost: " <<zwierzeObj.mWzrost << endl;
	cout << "Nazwa: " << zwierzeObj.mNazwa << endl;
	cout << "Gatunek: " <<zwierzeObj.mGatunek << endl;

	// ///////////////////////////////////////////////////////////////////////////////////////
	// Task 5.4 - Przeka� tablic� do funkcji i zlicz w funkcji ilo�� wyst�pie� liczby w tablicy, liczb� t� przeka� przez parametr

	int tablica[] {3,4,5,4,3,4,5,4,23,2,3,4,5,5};
	cout << countNumberEncountersInArray(tablica,(sizeof(tablica)/sizeof(*tablica)),5);

	// ///////////////////////////////////////////////////////////////////////////////////////
	// Task 5.5 - Obliczy�:. (w komentarzy wypisa�em m�j pierwszy strza� bez sprawdzania)

	int * i = new int[6]{1,2,3,4,5,6};

	cout << *(++i) << endl; // 2
	cout << *(i++) << endl; // 2
	cout << *(i+2) << endl; // 5
	cout << i << endl; // Adres 3
	cout << *i << endl; // 3
	cout << i[1] << endl; // 4
	cout << 2[i] << endl; // 6 - moja pomy�ka, poprawny wynik: 5

	// ///////////////////////////////////////////////////////////////////////////////////////
	// Task 5.6 - Napisa� w�asn� implementacj� funkcji realloc

    int * ptr = new int[10]{ 10, 4, 5, 6, 7, 3, 4, 6, 7, 99};
    ptr = static_cast<int*>(myRealloc(ptr, sizeof(int)*10, sizeof(int)*50));

	// ///////////////////////////////////////////////////////////////////////////////////////
	// Task 5.7 - Dynamiczn alokacja tablicy int.
	const int arrayHeight1 = 10;
	const int arrayWidth1 = 20;
	const int arrayHeight2 = 5;
	const int arrayWidth2 = 10;

	int ** twoDimArray1 = new int*[arrayHeight1];
	for (int index=0; index < arrayHeight1; ++index)
	{
		twoDimArray1[index] = new int[arrayWidth1]{};
	}
	for (int index=0; index < arrayHeight1; ++index)
	{
		delete[] twoDimArray1[index];
		twoDimArray1[index] = nullptr;
	}
	delete[] twoDimArray1;
	twoDimArray1 = nullptr;

	TwoDimArrayInOneSetVer2 twoDimArray2(arrayHeight2, arrayWidth2);
    cout << &twoDimArray2[0][9] << endl;
    cout << &twoDimArray2[1][0] << endl;

	// ///////////////////////////////////////////////////////////////////////////////////////
	// Task 5.8 - memcpy

	return 0;
}
