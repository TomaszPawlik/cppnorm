//============================================================================
// Name        : 1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

/*
 *
• Stworzyć zmienne typu int, double, char, unsigner int, uint8_t i przypisać im maksymalne wartości
• Stworzyć zmienną int_least8_t i wypisać jej rozmiar
• Obiczyć ilość elementów w tablicy: int16_t [8] przy użyciu sizeof
• Stworzyć tablicę charów 10 – elementową i zainicjalizować ją przy użyciu uniform initialization
 *
 * */

#include <iostream>
#include <stdint.h>
#include <limits>

using namespace std;

int main() {
	int varInt = numeric_limits<int>::max();
	double varDouble = numeric_limits<double>::max();
	char varChar = numeric_limits<char>::max();
	unsigned int varUInt = numeric_limits<unsigned int>::max();
	uint8_t varUInt8_t = numeric_limits<unsigned long long>::max();	//unsiged 8-bit integer

	cout << varInt << endl;
	cout << varDouble << endl;
	cout << (int)varChar << endl;
	cout << varUInt << endl;
	cout << (unsigned long long)varUInt8_t << endl;

	// ////////////////////////////////////////////////////////////////////////////

	int_least8_t varIntLeast8;										// smallest signed integer type with width of at least 8
	cout << sizeof(varIntLeast8) << endl;

	// ////////////////////////////////////////////////////////////////////////////

	int16_t int16_t_Array[8]{};
	cout << "Numer of elements: " << sizeof(int16_t_Array) / sizeof(int16_t_Array[0]) << endl;

	// ////////////////////////////////////////////////////////////////////////////

	char charArray[10] {};

	return 0;
}
