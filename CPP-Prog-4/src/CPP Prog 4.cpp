//============================================================================
// Name        : CPP.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>
#include <vector>
	#include <algorithm>
	#include <bits/stdc++.h>
using namespace std;

// Można też dodać dwa dodatkowe argumenty funkcji z referencją żeby zmienić ich wartość i w nich zapisać wynik
void squreFunction(double aA, double aB, double aC){
	double delta = pow(aB,2)-(4*aA*aC);
	if (delta < 0)
	{
		cout << "Delta < 0   -   nie ma miejsc zerowych." << endl;
	}
	else if (delta == 0)
	{
		double x0 = -aB/(2*aA);
		cout << "Delta == 0   -   jedno miejsce zerowe." << endl;
		cout << "Miejsce zerowe x0: " << x0 << endl;
	}
	else
	{
		double x1 = (-aB - sqrt(delta))/(2*aA);
		double x2 = (-aB + sqrt(delta))/(2*aA);
		cout << "Delta > 0   -   dwa miejsca zerowe." << endl;
		cout << "Miejsce zerowe x1: " << x1 << endl;
		cout << "Miejsce zerowe x2: " << x2 << endl;
	}
}

bool findElementInVector(vector<int> aVectorToCheck, int aValueToFind){
	for (vector<int>::iterator it = aVectorToCheck.begin(); it != aVectorToCheck.end(); ++it){
		if (aValueToFind == *it)
		{
			return true;
		}
	}
	return false;
}

unsigned int countEncountersInArray(int* aArrayToCheck,  int tableSize, int aNumberToCheck){
	unsigned int counter = 0;
	for (int i=0;i<tableSize;++i)
	{
		if (aNumberToCheck == *(aArrayToCheck+i))
			++counter;
	}
	return counter;
}

// ///////////////////////////////////////////////////////////////
// Rozwiazanie Artura
/*
int tabIntow[100];
for (int i = 0; i < 100; ++i) {
	tabIntow[i] = rand() % 11;
}

unordered_map<int,int> iCount;

for (int i = 0; i < 100; ++i) {
	iCount[tabIntow[i]]++;
}

for (auto u : iCount) {
	cout << u.first << " " << u.second << endl;
}
*/
// ///////////////////////////////////////////////////////////////

void encountersInArray(int* aArrayToCheck,  int tableSize){

	vector<int> listOfElementsInArray;

	for (int i=0;i<tableSize;++i)
	{
		if (!(find(listOfElementsInArray.begin(), listOfElementsInArray.end(), *(aArrayToCheck+i)) != listOfElementsInArray.end()))
		{
			listOfElementsInArray.push_back(*(aArrayToCheck+i));
		}
	}
	sort(listOfElementsInArray.begin(),listOfElementsInArray.end());

	int countTable[listOfElementsInArray.size()]{};

	for (int i=0;i<tableSize;++i)
	{
		for (int j=0;j<listOfElementsInArray.size();++j)
		{
			if (*(aArrayToCheck+i) == listOfElementsInArray[j])
				countTable[j]++;
		}
	}

	for (int i=0;i<listOfElementsInArray.size();++i)
		cout << listOfElementsInArray[i] << " : " << countTable[i] << endl;
}

unsigned int getUserOption(){
	unsigned int userChoice;
	do {
		cout << "Your option: ";
		cin >> userChoice;
		if (userChoice<0 || userChoice>5)
			cout << "Invalid input. Please repeat." << endl;
	} while (userChoice<0 || userChoice>5);
	return userChoice;
}

void displayMenu(){
	bool quit = false;
	while (!quit)
	{
	cout << "(1) Option 1" << endl
		 << "(2) Option 2" << endl
		 << "(3) Option 3" << endl
		 << "(4) Option 4" << endl
		 << "(5) Option 5" << endl
		 << "(0) Exit" << endl;

	switch (getUserOption()) {
		case 1:
			cout << "One" << endl;
			break;
		case 2:
			cout << "Two" << endl;
			break;
		case 3:
			cout << "Three" << endl;
			break;
		case 4:
			cout << "Four" << endl;
			break;
		case 5:
			cout << "Five" << endl;
			break;
		case 0:
			cout << "Goodbye!" << endl;
			quit = true;
			break;
		}
	}
}

double add(double aVal1, double aVal2=0.0){
	cout << "ADD: Double + Double: ";
	return aVal1 + aVal2;
}

double add(int aVal1, double aVal2=0.0){
	cout << "ADD: INT + Double ";
	return aVal1 + aVal2;
}

double add(double aVal1, int aVal2=0){
	cout << "ADD: Double + INT ";
	return aVal1 + aVal2;
}

int add(int aVal1, int aVal2=0){
	cout << "ADD: INT + INT ";
	return aVal1 + aVal2;
}

unsigned int factorialRecurency(unsigned int aVal){
	if (aVal==0)
		return 1;
	return factorialRecurency(aVal-1)*aVal;
}

template<typename T>
class MyArray{

public:

    MyArray(unsigned int aSize) : mSize(aSize){
        mTabPointer = new T[aSize];
    }

    ~MyArray(){
        delete[] mTabPointer;
    }

    T& operator[](unsigned int aIndex){
        if (aIndex >= mSize)
            throw "\"get\" method index error";
        else
            return *(mTabPointer+aIndex);
    }

    unsigned int size(){
        return mSize;
    }

    bool empty(){
        return (0 == mSize);
    }

    T get(unsigned int aIndex){
        if (aIndex >= mSize)
            throw "\"get\" method index error";
        else
            return *(mTabPointer+aIndex);
    }

    bool set(unsigned int aIndex, T aValue){
        if (aIndex >= mSize){
            throw "\"get\" method index error";
            return false;
        }
        else
        {
            *(mTabPointer+aIndex) = aValue;
        }
    }

    bool contains(T aValue){
        for (int i=0;i<mSize;++i)
        {
            if (*(mTabPointer+i) == aValue)
                return true;
        }
        return false;
    }

private:
    unsigned int mSize;
    T * mTabPointer;
};

int main() {

	// Task 4.1
	squreFunction(1,0,1);

	// ///////////////////////////////////////////////////////////////////////////////////////////////
	// Task 4.2
	vector<int> v1{5,6,9,2,3,5,1,6,8,3};
	int tab[]{1,4,3,5,6,7,3,4,5,1,2,4,7,9,3,1,2,5,6,1,3,4,9,4,5};
	cout << findElementInVector(v1, 4) << endl;
	cout << countEncountersInArray(tab,(sizeof(tab)/sizeof(*tab)),4) << endl;
	encountersInArray(tab,(sizeof(tab)/sizeof(*tab)));

	// ///////////////////////////////////////////////////////////////////////////////////////////////
	// Task 4.3
	displayMenu();

	// ///////////////////////////////////////////////////////////////////////////////////////////////
	// Task 4.4

	cout << add(7.2,1.2) << endl;
	cout << add(2,1.2) << endl;
	cout << add(7.2,1) << endl;
	cout << add(2,1) << endl;
	//cout << add(4.5); // Wyswietla error przy ustalonych domyslnych parametrach.
	//cout << add(2);	// Wyswietla error dla domyslnych parametrow

	// ///////////////////////////////////////////////////////////////////////////////////////////////
	// Task 4.5

	/*
	 * Stack - fast access memory reserved for program to keep function calls and their data.
	 * LIFO - last in, first out, method based on stack works
	 * Speed - access to data on stack is much faster than data at heap.
	 * Local variables - destroyed after exiting the function scope (they are not accessible by stack anymore, and they are removed from memory by OS after program end).
	 * Stack frame - frame of data that gets pushed onto the stack. Include function return addresses, function arguments, local variables and function return values
	 * Global variables are stored in other memory (static) - Data
	 * Heap - memory reserved for program by use of new, malloc etc (manual memory allocation).
	 */

	// ///////////////////////////////////////////////////////////////////////////////////////////////
	// Task 4.6
	cout << factorialRecurency(5) << endl;

	// ///////////////////////////////////////////////////////////////////////////////////////////////
	// Task 4.7 - std::array<int,5>2; - zaimlementowac.

	return 0;
}
