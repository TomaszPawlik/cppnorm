//============================================================================
// Name        : 2.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstring>	//memset
#include <string.h> //strlen
#include <math.h> // sqrt
using namespace std;

struct Zwierze{
	string gatunek;
	int wiek;
	string typ;
};

void countSigns(char charArray[]){
	unsigned int spolgloskiLiczba = 0;
	unsigned int samogloskiLiczba = 0;
	unsigned int bialeZnakiLiczba = 0;

    for (int i = 0; charArray[i]!='\0'; ++i)
    {
    	if (charArray[i] == 'a' || charArray[i] == 'e' || charArray[i] == 'i' || charArray[i] == 'o' || charArray[i] == 'u')
    		++samogloskiLiczba;
    	else if (charArray[i] >= 'a' && charArray[i] <= 'z')
    		++spolgloskiLiczba;
    	else
    		++bialeZnakiLiczba;
    }

    cout << "Samogloski: " << samogloskiLiczba << endl;
    cout << "Spolgloski: " << spolgloskiLiczba << endl;
    cout << "Biale znaki: " << bialeZnakiLiczba << endl;

};

//void bubbleSort(int (&tab)[], int n) - nie dziala dla gcc
//void bubbleSort(int *tab, int n) - zmieni wartosc tablicy
void bubbleSort(int tab[], int numberOfTabElements)
{

	cout << "Before sort: " << endl;
	for (int i = 0; i < numberOfTabElements; i++)
		cout << tab[i] << " ";
	cout << endl;


	for (int i = 1; i < numberOfTabElements; i++)
	{
		for (int j = numberOfTabElements - 1; j >= 1; j--)
		{
			if (tab[j] < tab[j - 1])
			{
				int bufor;
				bufor = tab[j - 1];
				tab[j - 1] = tab[j];
				tab[j] = bufor;
			}
		}
	}

	cout << "After sort: " << endl;
	for (int i = 0; i < numberOfTabElements; i++)
		cout << tab[i] << " ";
	cout << endl;
}

class A {

	int a;
	bool b;
	short c;
	double d;
	int* e;
};

class B {

	bool a;
	bool b;
	bool c;
	short d;
	int* e;
	bool * f;
};

class C {
	long double a;
	int* b;
	bool c;
	float d;
	int* e;
	int* f;
};

class __attribute__((packed)) D {
	long double a;
	int* b;
	bool c;
	float d;
	int* e;
	int* f;
};

int main() {

	// (+) Task 2.2
	// ////////////////////////////////////////////////////////////////////////////
	// const - zmienna ktorej wartoc nie bedzie zmieniona, co kompilator moze wykorzystac do optymalizacji
	// constexpr - Słowo kluczowe constexpr gwarantuje, że wartość zwracana przez funkcję, metodę bądź zmienną jest
	//			   stała podczas procesu kompilacji.
	// DEFINE - makro preprocesora

	// ////////////////////////////////////////////////////////////////////////////

	int intArray[10]{};
	int intArraySecond[10];

	memset(intArraySecond,0,sizeof(intArraySecond));

    memset(intArraySecond,0x01,sizeof(intArraySecond));

	for (auto numb : intArraySecond)
	{
		memset(&numb,0,3);
		numb = numb >> 22;
		std::cout << numb;
		//std::cout << std::hex << numb;
	}
	std::cout << std::endl;
	//std::cout << std::dec <<sizeof(intArraySecond) << std::endl;

	// ////////////////////////////////////////////////////////////////////////////

	Zwierze z1{"Owczarek niemiecki",3,"Pies"};
	z1.gatunek = "Owczarek kaukaski";
	z1.wiek = 4;

	// ////////////////////////////////////////////////////////////////////////////

	char charArray1[] = "Zdanie Numer 1";
	char charArray2[strlen(charArray1)];
	strcpy(charArray2,charArray1);

//	for (char c : charArray2)
//		cout << c;
//	cout << strlen(charArray1) << endl;

	// ////////////////////////////////////////////////////////////////////////////
	// (+) Task 2.3

	// (!) mozna zrobic string dla samoglosek i spawdzac czy dana litera zawiera sie w stringu (rozwiazanie Kuby)
	char content[] = "ala ma kota a kot ma ale";
	countSigns(content);

	// ////////////////////////////////////////////////////////////////////////////
	// (+) Task 2.4

	int TwoDimArray[10][10]{};

	// Zadziala jednie gdy szerokosc i wysokosc dla tablicy dwuwymiarowej sa takie same
	int widthAndHeightofTab = sqrt((sizeof(TwoDimArray)/sizeof(**TwoDimArray)));	// ilosc elementow dla width / height

	for (int i=0;i<widthAndHeightofTab;i++){
		for (int j=0;j<widthAndHeightofTab;j++)
			TwoDimArray[i][j]=i*j;
	}

	for (int i=0;i<widthAndHeightofTab;i++){
		for (int j=0;j<widthAndHeightofTab;j++)
			cout << TwoDimArray[i][j] << " ";
		cout << endl;
	}

	// ////////////////////////////////////////////////////////////////////////////
	// (+) Task 2.5

	cout << "Size of class A: " << sizeof(A) << endl;
	cout << "Size of class B: " << sizeof(B) << endl;
	cout << "Size of class C: " << sizeof(C) << endl;
	cout << "Size of class D: " << sizeof(D) << endl;

	// ////////////////////////////////////////////////////////////////////////////
	// (+) Task 2.6

	int tabToSort[] = {6,4,3,9,2,1,5,7,8};
	bubbleSort(tabToSort,sizeof(tabToSort)/sizeof(*tabToSort));

	return 0;
}
