//============================================================================
// Name        : CPP.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdint.h> // uint8_t
//#include <bits/stdc++.h> // bitset
#include <bitset>
using namespace std;

#define bit0 0b00000001
#define bit1 0b00000010
#define bit2 0b00000100
#define bit3 0b00001000
#define bit4 0b00010000
#define bit5 0b00100000
#define bit6 0b01000000
#define bit7 0b10000000

int bitCount(unsigned int aNumberCountBit){
	int result = 0;
	while (aNumberCountBit != 0){
		if (aNumberCountBit&0b01)
			++result;
		aNumberCountBit >>= 1;
	}
	return result;
}

int bitCountWithBitSet(unsigned int aNumberCountBit){
	int result = 0;
	bitset<8> b(aNumberCountBit);
	for (int i=0;b != 0; b.reset(i),i++)
	{
		if (b[i] == 0b01)
		{
			++result;
		}
	}
	return result;
}

int main() {
	// (+) Task 3.1
	// ////////////////////////////////////////////////////////////////////////////

	// Unary operators: unary minus(-) / increment(++) / decrement(--) / NOT(!) / Addressof operstor(&) / sizeof()
	// Ternary operator:  (expression 1) ? expression 2 (done if true) : expression 3 (done if false)

	/*
	 ==		binary
	 !=		binary
	 >		binary
	 <		binary
	 <=		binary
	 -		unary / binary
	 &		unary / binary
	 >>		binary
	 =		binary
	 *=		binary
	 +=		binary
	 >>=	binary
	 |=		binary
	*/

	double douleValue = 4.5;
	int intValue;
	intValue = (int)douleValue;

	// (+) Task 3.2
	// ////////////////////////////////////////////////////////////////////////////

	/*
	 278 - 0b000100010110 - 0x116
	 0x1BC - 0b000110111100 - 444
	 0b11010010101 - 0x695 - 1685
	*/

	// (+) Task 3.3
	// ////////////////////////////////////////////////////////////////////////////

	/*
	7 & 17 = 1
	9 | 23 = 31
	122 ^ 100 = 30
	*/

	uint8_t unit = 134u;
	unit|=(bit3 | bit5 | bit7);
	cout << (int)unit << endl;

	unit = 134u;
	unit&=~(bit0 | bit1 | bit2 | bit3);
	cout << (int)unit << endl;

	unit = 134u;
	unit^=(bit4 | bit5 | bit6);
	cout << (int)unit << endl;

	cout << "++++++++++++++++++++++++++++++++++++++++++" << endl;

	bitset<8> b(134);
	cout << b.to_ulong() << endl;

	cout << "++++++++++++++++++++++++++++++++++++++++++" << endl;

	b.set(3);b.set(5);b.set(7);
	cout << b.to_ulong() << endl;

	b = 134;
	b.reset(0);b.reset(1);b.reset(2);b.reset(3);
	cout << b.to_ulong() << endl;

	b = 134;
	b.flip(4);b.flip(5);b.flip(6);
	cout << b.to_ulong() << endl;

	// (+) Task 3.4
	// ////////////////////////////////////////////////////////////////////////////

	cout << "Task 3.4-1: " << bitCount(7) << endl;
	cout << "Task 3.4-2: " << bitCountWithBitSet(15) << endl;

	return 0;
}
