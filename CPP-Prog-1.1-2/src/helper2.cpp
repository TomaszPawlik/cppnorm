 /*
 * helper2.cpp
 *
 *  Created on: Mar 11, 2019
 *      Author: tpawlik
 */

#include "headers/helper2.h"
#include "headers/printer.h"

void helper2()
{
	printer();
	std::cout << "helper2" << std::endl;
}


