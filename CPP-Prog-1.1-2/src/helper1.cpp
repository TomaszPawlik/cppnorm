/*
 * helper1.cpp
 *
 *  Created on: Mar 11, 2019
 *      Author: tpawlik
 */

#include "headers/helper1.h"
#include "headers/printer.h"

void helper1()
{
	printer();
	std::cout << "helper1" << std::endl;
}


