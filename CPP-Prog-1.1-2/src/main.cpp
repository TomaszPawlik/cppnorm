
#include <iostream>

// (c) Działa normalnie: (wariant 1)
#include "headers/helper1.h"
#include "headers/helper2.h"

// (c) Działa po zaincludowaniu sciezki w opcjach projktu: (wariant 2)
//#include <helper1.h>
//#include <helper2.h>

#define SQUARE(x) ((x)*(x))

int main() {

	// Task 1

	std::cout << SQUARE(4+3) << std::endl;

	// Task 2
	// g++ -o task1.out main.cpp helper1.cpp helper2.cpp printer.cpp				(wariant 1)
	// g++ task1.2.out -I headers/ main.cpp helper1.cpp helper2.cpp printer.cpp		(wariant 2)

	helper1();
	helper2();

	return 0;
}
